package com.fotorest.perfil;

import org.springframework.data.repository.CrudRepository;

public interface PerfilRespository extends CrudRepository<Perfil, String>{

}
