package com.fotorest.perfil;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PerfilService {
	@Autowired
	PerfilRespository perfilRepository;
	
	@Autowired
	UsuarioClient usuarioClient;
	
	public Optional<Perfil> buscar(String nome){
		return perfilRepository.findById(nome);
	}
	
	public Perfil inserir(Perfil perfil) {
		usuarioClient.inserir(perfil);
		return perfilRepository.save(perfil);
	}
}
